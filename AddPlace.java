package googleAPI;

import core.utilities.RestServiceHelper;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class AddPlace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		RestServiceHelper restserviceHelper=new RestServiceHelper();
		Response response;
		String baseURL="https://rahulshettyacademy.com";
		restserviceHelper.initiateRequest();
		restserviceHelper.setbaseURI(baseURL);
		restserviceHelper.setQueryparam("key", "qaclick123");
		restserviceHelper.setHeader("Content-Type","application/json");
		restserviceHelper.generatePostBodyFromJSONObject(Payload.payloadAdd());
		//restserviceHelper.setPostResourse("/maps/api/place/add/json").then().log().all().statusCode(200);
		response=restserviceHelper.setPostResourse("/maps/api/place/add/json");
		restserviceHelper.checkStatusCodeIs200(response);
		

		//int statuscode=response.statusCode();
		
		//restserviceHelper.checkStatusCodeIs200();

	}



}


